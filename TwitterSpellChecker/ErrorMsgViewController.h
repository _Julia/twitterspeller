//
//  ErrorMsgViewController.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorMsgViewController : UIAlertController

+ (void) showMessage : (NSString *) titleText message:(NSString *) msgText;

@end
