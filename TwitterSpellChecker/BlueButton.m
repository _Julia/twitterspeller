//
//  BlueButton.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "BlueButton.h"



@implementation BlueButton


- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius = 3;
    self.clipsToBounds = YES;
}


@end
