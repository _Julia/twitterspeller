//
//  NetworkManager+SpellChecker.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "NetworkManager.h"

@interface NetworkManager (SpellChecker)

- (void)checkTextsRequest:(NSArray *)texts completion:(void (^)(BOOL success, id result))completion;
- (void)checkTextRequest:(NSString *)text completion:(void (^)(BOOL success, id result))completion;


@end
