//
//  WriteTweetViewController.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright © 2015 Julia Ponomareva. All rights reserved.
//

#import "WriteTweetViewController.h"
#import "NetworkManager+SpellChecker.h"
#import "SpellStringsFormatter.h"
#import <TwitterKit/TwitterKit.h>
#import "ErrorMsgViewController.h"
#import "Const.h"



@interface WriteTweetViewController () <UITextViewDelegate>

@end

@implementation WriteTweetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tweetTextView.delegate = self;
    [self.tweetTextView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - text view

- (void)textViewDidChange:(UITextView *)textView
{
    int count = [self.tweetTextView possibleSymbolsCount];
    self.countSymbLabel.text = [NSString stringWithFormat:NSLocalizedString(@"SYMB_PATTERN", nil), count];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    self.tweetTextView.textColor = [UIColor blackColor];
    int count = [self.tweetTextView possibleSymbolsCount];
    
    if (count) {
        return YES;
    } else {
        return NO;
    }
}


#pragma mark - button actions
- (IBAction)tweetBtnTap:(id)sender {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [[Twitter sharedInstance]logInWithCompletion:^(TWTRSession *session, NSError *error) {
        TWTRAPIClient *client = [[Twitter sharedInstance]APIClient];
        NSURLRequest *tweetRequest = [client URLRequestWithMethod:@"POST" URL:UPDATE_URL parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.tweetTextView.text, @"status", nil] error:&error];
        
        [client sendTwitterRequest:tweetRequest completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (connectionError) {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"SEND_TWEET_ERROR", nil)];
            } else {
                [self performSegueWithIdentifier:@"goToMainFeed" sender:self];
            }
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }];
    }];
}

- (IBAction)checkBtnTap:(id)sender {
    __weak typeof(self)weakSelf = self;
    
    [[NetworkManager sharedNetworkManager] checkTextRequest:self.tweetTextView.text completion:^(BOOL success, id result) {
        __strong typeof(self)self = weakSelf;
        if (success) {
            self.tweetTextView.textColor = [UIColor greenColor];
            self.tweetTextView.text = [SpellStringsFormatter checkAndReplaceErrors:self.tweetTextView.text withArray:result andTweetObject:nil];
            
        } else {
            self.tweetTextView.textColor = [UIColor blackColor];
        }
    }];
}



@end
