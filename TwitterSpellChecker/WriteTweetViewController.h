//
//  WriteTweetViewController.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright © 2015 Julia Ponomareva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlueButton.h"
#import "TweetTextView.h"

@interface WriteTweetViewController : UIViewController


@property (weak, nonatomic) IBOutlet BlueButton *tweetButton;

@property (weak, nonatomic) IBOutlet UILabel *countSymbLabel;

@property (weak, nonatomic) IBOutlet TweetTextView *tweetTextView;


@end
