//
//  TweetTextView.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import <UIKit/UIKit.h>



extern const int MAX_SYMB_COUNT;

@interface TweetTextView : UITextView


- (int) possibleSymbolsCount;


@end
