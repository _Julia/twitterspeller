//
//  NetworkManager.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kSpellerServiceTextsUrl = @"http://speller.yandex.net/services/spellservice.json/checkTexts";
static NSString * const kSpellerServiceTextUrl = @"http://speller.yandex.net/services/spellservice.json/checkText";

typedef enum{
    GET,
    POST,
    PUT,
    DELETE
} RequestType;

@interface NetworkManager : NSObject

+(instancetype) sharedNetworkManager;

- (void)requestType:(RequestType)type url:(NSString *)url parameters:(NSDictionary *)parameters completion:(void (^)(BOOL success, id result))completion;

- (BOOL)connected;

@end
