//
//  MainFeedViewController.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright © 2015 Julia Ponomareva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainFeedViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *feedTableView;

@property (strong, nonatomic) NSMutableArray *tweetsArray;

@end
