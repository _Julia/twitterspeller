//
//  ErrorMsgViewController.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "ErrorMsgViewController.h"
#import "AppDelegate.h"

@interface ErrorMsgViewController ()

@end

@implementation ErrorMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

+ (void)showMessage:(NSString *)titleText message:(NSString *)msgText {
    UIViewController *vc =(UIViewController*)[[(AppDelegate*)[[UIApplication sharedApplication]delegate] window] rootViewController];
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:titleText  message:msgText  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [vc dismissViewControllerAnimated:YES completion:nil];
    }]];
    [vc presentViewController:alertController animated:YES completion:nil];
    [vc resignFirstResponder];
}

@end
