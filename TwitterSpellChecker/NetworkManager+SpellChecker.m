//
//  NetworkManager+SpellChecker.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "NetworkManager+SpellChecker.h"
#import "ErrorMsgViewController.h"

@implementation NetworkManager (SpellChecker)

- (void)checkTextsRequest:(NSArray *)texts completion:(void (^)(BOOL success, id result))completion {
    NSString *url = kSpellerServiceTextsUrl;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSError * error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:texts options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [params setObject:jsonString forKey:@"text"];
    [params setObject:@(23) forKey:@"options"];

    [self requestType:POST url:url parameters:params completion:^(BOOL success, id result){
        if (!success) {
            if ([[NetworkManager sharedNetworkManager] connected]) {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"SPELLER_CHECK_ERROR", nil)];
            } else {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"CHECK_NETWORK_STATUS", nil)];
            }
            
            completion(NO, result);
            return;
        }
        if (!result) {
            if ([[NetworkManager sharedNetworkManager] connected]) {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"SPELLER_CHECK_ERROR", nil)];
            } else {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"CHECK_NETWORK_STATUS", nil)];
            }

            completion(NO,result);
            return;
        }

        NSArray *resultArr = (NSArray *) result;
        if (resultArr.count > 0) {
            resultArr = [resultArr objectAtIndex:0];
        }
        completion(YES,resultArr);
        
    }];
}



- (void)checkTextRequest:(NSString *)text completion:(void (^)(BOOL success, id result))completion {
    NSString *url = kSpellerServiceTextUrl;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:text forKey:@"text"];
    [params setObject:@(23) forKey:@"options"];
    
    [self requestType:POST url:url parameters:params completion:^(BOOL success, id result){
        if (!success) {
            if ([[NetworkManager sharedNetworkManager] connected]) {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"SPELLER_CHECK_ERROR", nil)];
            } else {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"CHECK_NETWORK_STATUS", nil)];
            }

            completion(NO, result);
            return;
        }
        
        if (!result) {
            if ([[NetworkManager sharedNetworkManager] connected]) {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"SPELLER_CHECK_ERROR", nil)];
            } else {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"CHECK_NETWORK_STATUS", nil)];
            }

            completion(NO,result);
            return;
        }
        
        completion(YES,result);
    }];
}

@end

