//
//  TweetTextView.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "TweetTextView.h"

const int MAX_SYMB_COUNT = 140;

@implementation TweetTextView


//max 140 symb
- (int) possibleSymbolsCount {
    return MAX_SYMB_COUNT - (int)self.text.length;
}


@end
