//
//  BlueButton.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface BlueButton : UIButton

@end
