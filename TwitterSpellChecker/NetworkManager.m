//
//  NetworkManager.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "AppDelegate.h"

@implementation NetworkManager

static NetworkManager *networkManager;

+(instancetype) sharedNetworkManager{
    @synchronized (self){
        static dispatch_once_t onceToken;
        if (!networkManager){
            dispatch_once(&onceToken, ^{
                networkManager = [[NetworkManager alloc] init];
            });
        }
        return networkManager;
    }
}

- (void)requestType:(RequestType)type url:(NSString *)url parameters:(NSDictionary *)parameters completion:(void (^)(BOOL success, id result))completion {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    void (^successBlock)(AFHTTPRequestOperation * operation, id responseObject) = ^(AFHTTPRequestOperation * operation, id responseObject){

        completion(YES, responseObject);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    };
    
    void (^failureBlock)(AFHTTPRequestOperation * operation, NSError * error) = ^(AFHTTPRequestOperation * operation, NSError * error){

        completion(NO, error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    };
    
    AFHTTPRequestOperation *operation;
    switch(type){
        case GET:
            operation = [manager GET:url parameters:parameters success:successBlock failure:failureBlock];
            break;
        case POST:
            manager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
            operation = [manager POST:url parameters:parameters success:successBlock failure:failureBlock];
            break;
        default:
            break;
    }
}


- (BOOL)connected {
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

@end
