//
//  TweetFeedTableViewCell.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright © 2015 Julia Ponomareva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TweetObject.h"

@interface TweetFeedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetTextLabel;


- (void) fillCell:(TweetObject *)tweet;

@end
