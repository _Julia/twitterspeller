//
//  LoginViewController.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "LoginViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "ErrorMsgViewController.h"
#import "NetworkManager.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //check session
    if ([Twitter sharedInstance].session) {
        [self performSegueWithIdentifier:@"showMainFeedVC" sender:self];
    } else {
        TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
            if (session.authToken && session.authTokenSecret && session.userID) {
                [self performSegueWithIdentifier:@"showMainFeedVC" sender:self];
            } else {
                if ([[NetworkManager sharedNetworkManager] connected]) {
                    [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"AUTH_ERROR", nil)];
                } else {
                    [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"CHECK_NETWORK_STATUS", nil)];
                }
            }
        }];
        logInButton.center = self.view.center;
        [self.view addSubview:logInButton];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
