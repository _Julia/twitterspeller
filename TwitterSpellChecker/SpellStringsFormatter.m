//
//  SpellStringsFormatter.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "SpellStringsFormatter.h"
#import "NSMutableArray+SafetyObject.h"
#import <TwitterKit/TwitterKit.h>


@implementation SpellStringsFormatter

//format income text
+ (NSString *) checkAndReplaceErrors : (NSString *) str withArray:(NSArray*) array andTweetObject:(TweetObject*)tweet{
    NSString *result = str;
    for (NSDictionary *dictionary in array) {
        NSString *errorWord = [dictionary objectForKey:@"word"];

        NSArray *correctedArray = [dictionary objectForKey:@"s"];
        NSString *correctedWord;
        if (correctedArray.count > 0) {
             correctedWord = [correctedArray objectAtIndex:0];
        }
        if (errorWord && correctedWord && [result rangeOfString:errorWord].location != NSNotFound) {
            tweet.isCorrected = YES;
            result = [result stringByReplacingOccurrencesOfString:errorWord withString:correctedWord];
        }
    }
    return result;
}


//format array tweet texts
+ (NSArray *)formTweetTextsArray: (NSArray *)tweetsArray {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (TWTRTweet *tweet in tweetsArray) {
        [result setSafetyObject:tweet.text];
    }
    return result;
}

@end
