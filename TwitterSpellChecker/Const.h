//
//  Const.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#ifndef TwitterSpellChecker_Const_h
#define TwitterSpellChecker_Const_h

static NSString * const HOME_TIMELINE_URL = @"https://api.twitter.com/1.1/statuses/home_timeline.json";
static NSString * const UPDATE_URL = @"https://api.twitter.com/1.1/statuses/update.json";

#endif
