//
//  TweetObject.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TweetObject : NSObject

@property (strong, nonatomic) NSString *tweetId;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *nickname;
@property (strong, nonatomic) NSString *tweetOriginalText;
@property (strong, nonatomic) NSString *tweetCorrectedText;
@property (assign) BOOL isCorrected;



- (instancetype) initWith: (NSString *)tweetId userName:(NSString *)username nickname:(NSString *)nickname originalText:(NSString *)originalText;

@end
