//
//  MainFeedViewController.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright © 2015 Julia Ponomareva. All rights reserved.
//

#import "MainFeedViewController.h"
#import "TweetFeedTableViewCell.h"
#import <TwitterKit/TwitterKit.h>
#import "NetworkManager+SpellChecker.h"
#import "TweetObject.h"
#import "NetworkManager+SpellChecker.h"
#import "SpellStringsFormatter.h"
#import "ErrorMsgViewController.h"
#import "Const.h"



@interface MainFeedViewController () <UITableViewDataSource, UITableViewDelegate>


@end

@implementation MainFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tweetsArray = [[NSMutableArray alloc] init];
    
    self.feedTableView.delegate = self;
    self.feedTableView.dataSource = self;
    
    self.feedTableView.estimatedRowHeight = 150.0;
    self.feedTableView.rowHeight = UITableViewAutomaticDimension;

}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getUserHomeTimeline];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) unwindToMainFeed:(UIStoryboardSegue *)segue {
    
}



#pragma mark - table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tweetsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TweetFeedTableViewCell *tweetCell = (TweetFeedTableViewCell *)[self.feedTableView dequeueReusableCellWithIdentifier:@"TweetFeedCell"];
    
    if (tweetCell == nil) {
        tweetCell = [[TweetFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TweetFeedCell"];
    }
    
    [tweetCell fillCell:self.tweetsArray[indexPath.row]];
    return tweetCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


#pragma mark - twitter request 

-(void) getUserHomeTimeline
{
    TWTRAPIClient *client = [[Twitter sharedInstance]APIClient];
    NSString *statusesShowEndpoint = HOME_TIMELINE_URL;
    NSDictionary *params = @{@"count": @"10"};
    NSError *clientError;
    
    NSURLRequest *request = [[[Twitter sharedInstance] APIClient] URLRequestWithMethod:@"GET" URL:statusesShowEndpoint parameters:params error:&clientError];
    
    if (request) {
        __weak typeof(self)weakSelf = self;
        [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                NSArray *arrayTweets = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                arrayTweets = [NSMutableArray arrayWithArray:[TWTRTweet tweetsWithJSONArray:arrayTweets]];
                
                __strong typeof(self)self = weakSelf;
                [self.tweetsArray removeAllObjects];
                
                if (arrayTweets.count > 0) {
                    [self renderTweetsWithoutErrors:arrayTweets];
                }
            }
            else {
                [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"CHECK_NETWORK_STATUS", nil)];
            }
        }];
    }
    else {
        [ErrorMsgViewController showMessage:nil message:NSLocalizedString(@"FEED_LOAD_ERROR", nil)];
    }
}


- (void) renderTweetsWithoutErrors : (NSArray *)arrayTweets {
    __weak typeof(self)weakSelf = self;
    [[NetworkManager sharedNetworkManager] checkTextsRequest:[SpellStringsFormatter formTweetTextsArray:arrayTweets] completion:^(BOOL success, id result) {
        __strong typeof(self)self = weakSelf;

        for (TWTRTweet *tweet in arrayTweets) {
            TweetObject *tweetObject = [[TweetObject alloc] initWith:tweet.tweetID userName:tweet.author.name nickname:tweet.author.screenName originalText:tweet.text];
            if (success && result) {
                tweetObject.tweetCorrectedText = [SpellStringsFormatter checkAndReplaceErrors:tweet.text withArray:result andTweetObject:tweetObject];
            }
            [self.tweetsArray addObject:tweetObject];
            [self.feedTableView reloadData];
        }
    }];
}


@end
