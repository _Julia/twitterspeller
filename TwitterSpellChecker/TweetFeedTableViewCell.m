//
//  TweetFeedTableViewCell.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright © 2015 Julia Ponomareva. All rights reserved.
//

#import "TweetFeedTableViewCell.h"

@implementation TweetFeedTableViewCell

- (void)awakeFromNib {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (void) fillCell:(TweetObject*)tweet {
    self.userNameLabel.text= tweet.userName;
    self.nicknameLabel.text = [@"@" stringByAppendingString:tweet.nickname];
    
    if (tweet.isCorrected) {
        self.tweetTextLabel.text = tweet.tweetCorrectedText;
        self.tweetTextLabel.textColor = [UIColor greenColor];
    } else {
        self.tweetTextLabel.text = tweet.tweetOriginalText;
        self.tweetTextLabel.textColor = [UIColor blackColor];
    }
    
}

@end
