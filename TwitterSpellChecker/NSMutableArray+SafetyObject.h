//
//  NSMutableArray+SafetyObject.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (SafetyObject)

- (void) setSafetyObject:(id)anObject;

@end
