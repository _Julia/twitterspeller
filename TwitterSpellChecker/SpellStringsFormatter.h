//
//  SpellStringsFormatter.h
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TweetObject.h"


@interface SpellStringsFormatter : NSObject


+ (NSString *) checkAndReplaceErrors : (NSString *) str withArray:(NSArray*) array andTweetObject:(TweetObject*)tweet;
+ (NSArray *) formTweetTextsArray: (NSArray *)tweetsArray;

@end
