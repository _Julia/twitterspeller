//
//  TweetObject.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 29.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "TweetObject.h"

@implementation TweetObject

- (instancetype) initWith: (NSString *)tweetId userName:(NSString *)username nickname:(NSString *)nickname originalText:(NSString *)originalText{
    self = [super init];
    if (self) {
        self.tweetId = tweetId;
        self.userName = username;
        self.nickname = nickname;
        self.tweetOriginalText = originalText;
        self.tweetCorrectedText = nil;
        self.isCorrected = NO;
        
    }
    return self;
    
}


@end
