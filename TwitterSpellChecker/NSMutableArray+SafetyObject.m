//
//  NSMutableArray+SafetyObject.m
//  TwitterSpellChecker
//
//  Created by Julia Ponomareva on 30.09.15.
//  Copyright (c) 2015 Julia Ponomareva. All rights reserved.
//

#import "NSMutableArray+SafetyObject.h"

@implementation NSMutableArray (SafetyObject)

- (void) setSafetyObject:(id)anObject {
    if (anObject) {
        [self addObject:anObject];
    }
}

@end
